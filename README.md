# Contexte du projet

Le concours se déroulera à Carmaux le 12 juin prochain. Les participants devront s'inscrire au minimum un mois avant l'événement en envoyant un message à rouletaboule@cornomail.net et préciser les équipes complètes (2 ou 4 personnes majeures obligatoirement), ainsi que leur club d'affiliation, s'il en sont adhérant.

La charte graphique est assez libre : le projet doit être sur une base de vert et marron avec une police sans empattement. Le noir est interdit pour les textes, et une illustration doit être inclue. Bien entendu, la création doit être absolument magnifique et donner une envie irrésistible de venir à l'événement, qu'on soit joueur ou non :)

L'engagement est fixé à 10€ pour les licenciés, et 15€ pour les autres. La logistique sur place permettra de se restaurer et se désaltérer. Un espace sanitaire sera à disposition. Pas de distributeur de billet sur place.

# Livrable

- Lien du site en ligne : unique-floor.surge.sh

# Maquette

- site = version normal
Image en fond
- mobile = version mobile
Image en fond caché 


